﻿using eWare;
using Microsoft.Win32;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace Dublin_Chamber.Enbu.Includes
{
    public static class SpreadSheet
    {


        //Public Methods==================================================================================================================================================================

        //Creates DataTable from an eWare object and any valid SQL Statement
        public static DataTable SelectSQL(String sSQLStatement, eWareBase eWareInstance)
        {
            eWareQuery SQLQuery = eWareInstance.CreateQueryObj(sSQLStatement);
            SQLQuery.SelectSql();

            // Create DataTable
            DataTable dataTable = new DataTable();

            dynamic recordset = SQLQuery.Cast<string>();
            IEnumerator SQLEnum = SQLQuery.GetEnumerator();

            while (SQLEnum.MoveNext())
            {
                string ColumnName = SQLEnum.Current.ToString();
                dataTable.Columns.Add(ColumnName);
            }

            while (!SQLQuery.eof)
            {
                DataRow row = dataTable.NewRow();
                SQLEnum.Reset();
                while (SQLEnum.MoveNext())
                {
                    string ColumnName = SQLEnum.Current.ToString();
                    row[ColumnName] = SQLQuery[ColumnName];
                }
                dataTable.Rows.Add(row);
                SQLQuery.NextRecord();
            }

            return dataTable;

        }

        //Converts DataTable to HTML String, requires Datatable and list of Fields required - optionally include previous fields if they have been calculated, and styles                
        public static String GetHTMLFromDataTable(DataBlock edo, String rowColour1 = "#DEDEE7", String rowColour2 = "#FFFFFF")
        {
            //remove unneeded fields
            SpreadSheet.FormatColumns(edo, edo.showPrevious);

            //Start building return String
            string html = "<table width='100%' cellpadding='4' style='border-collapse: collapse' id='results'>";

            //Add heading block if required
            if (edo.titleString != "")
            {
                html += createHTMLrow(true, 0);
                html += createHTMLCell(true, edo.titleString, true, edo.dataTable.Columns.Count);
                html += "</tr>";
                               
                //Create Subtitle if Applicable
                foreach(String s in edo.SubTitles)
                {
                    html += createHTMLrow(true, 0);
                    html += createHTMLCell(true, s, true, edo.dataTable.Columns.Count);
                    html += "</tr>";
                }
            }           

            //Create header TR 
            html += createHTMLrow(true, 0);  //tr /*--------------------------------------------------------- Create Header Table Row */

            //Create header TD's 
            foreach (DataColumn dc in edo.dataTable.Columns)
            {
                String headerLabel = getColumnHeader(dc.ColumnName, edo.fieldCollection.fieldList);
                Boolean isVisible = isColumnVisible(dc.ColumnName, edo.fieldCollection.fieldList);
                html += createHTMLCell(true, headerLabel, isVisible);
            }
            html += "</tr>";

            //Create Table Data
            for (int i = 0; i < edo.dataTable.Rows.Count; i++)
            {
                //Create data row
                html += createHTMLrow(false, i);           /*------------------------------------------------------- Create Data table Row */
                //Create data cells
                for (int j = 0; j < edo.dataTable.Columns.Count; j++)
                {
                    //Check if column required exists in data table
                    DataColumn dc = edo.dataTable.Columns[j];

                    String data2Display = edo.dataTable.Rows[i][j].ToString();
                    Boolean isVisible = isColumnVisible(dc.ColumnName, edo.fieldCollection.fieldList);
                    html += createHTMLCell(false, data2Display, isVisible); /*------------------ Create Data Table Cells */

                }

                html += "</tr>";
            }

            html += "</table>";
            return html;
        }

        //Converts Data table to Spreadsheet and returns the byte array of the Spreadsheet - optionally include previous fields if they have been calculated
        public static byte[] ConvertDataTableToExcel(DataBlock edo, String StartingCell, String WorksheetName, Boolean formatOutput = true, String columnHeaderColour = "#B3B3B3", String rowColour1 = "#DEDEE7", String rowColour2 = "#FFFFFF", String titleColour = "#B8CCE4")
        {
            //CreateNew SpreadSheet
            ExcelPackage ep = new ExcelPackage();
            ExcelWorksheet ws = ep.Workbook.Worksheets.Add(WorksheetName);

            addWorksheetData(ws, edo, StartingCell, formatOutput, columnHeaderColour,rowColour1,rowColour2,titleColour);

            return ep.GetAsByteArray();
        }

        //Converts a list of EnbuWorksheet Objects into a HTML String - each EnbuWorksheetObject contains a list of datatables and their respective field list
        public static String ConvertReportToHTML(List<EnbuWorkSheet> leds, String WorkSheetSeperator = "<br>")
        {
            String result = "";

            //Check each WorkSheet
            foreach (EnbuWorkSheet eds in leds)
            {
                //Check each datatable in the current worksheet
                foreach (DataBlock edo in eds.DataObjects)
                {

                        result += GetHTMLFromDataTable(edo);
                    result += "<br>";
                }

                result += WorkSheetSeperator;

            }


            return result;
        }

        //Converts a list of EnbuWorksheet Objects to a spreadsheet with multiple worksheets - each EnbuWorksheetObject contains a list of datatables and their respective field list
        public static byte[] ConvertReportToExcel(List<EnbuWorkSheet> leds, Boolean FormatOutput, String StartingCell = "A1", int Spacing = 1, String columnHeaderColour = "#B3B3B3", String rowColour1 = "#DEDEE7", String rowColour2 = "#B8CCE4")
        {

            //Create new Worksheet and set its file info
            ExcelPackage ep = new ExcelPackage();

            //For each Worksheet
            foreach (EnbuWorkSheet ebs in leds)
            {
                // Create the worksheet and add it to the Excel Object
                ExcelWorksheet ws = ep.Workbook.Worksheets.Add(ebs.workSheetname);

                //Get the overall starting point for the spreadsheet
                String cellPosition = ebs.startingCell;

                //Foreach dataset in a worksheet - send that to the current worksheet
                for (int i = 0; i < ebs.DataObjects.Count; i++)
                {
                    addWorksheetData(ws, ebs.DataObjects[i], cellPosition, FormatOutput, "#B3B3B3", "#DEDEE7", "#FFFFFF", "#B8CCE4");

                    //Calculate Position of Next Block
                    cellPosition = calculateNewStartingPosition(cellPosition, ebs.DataObjects[i].dataTable.Rows.Count, 0, Spacing);

                }

            }

            //return bytearray of the spreadsheet
            return ep.GetAsByteArray();

        }
          
        //Determine The content type of the response
        public static string GetFileContentType(string fileextension)
        {
            //set the default content-type
            const string DEFAULT_CONTENT_TYPE = "application/unknown";


            RegistryKey regkey, fileextkey;
            string filecontenttype;


            //the file extension to lookup
            //fileextension = ".zip";


            try
            {
                //look in HKCR
                regkey = Registry.ClassesRoot;


                //look for extension
                fileextkey = regkey.OpenSubKey(fileextension);


                //retrieve Content Type value
                filecontenttype = fileextkey.GetValue("Content Type", DEFAULT_CONTENT_TYPE).ToString();


                //cleanup
                fileextkey = null;
                regkey = null;
            }
            catch
            {
                filecontenttype = DEFAULT_CONTENT_TYPE;
            }


            //print the content type
            return filecontenttype;


        }

        //Create a simple fixed 4 columns datatable with 5 rows of sample data
        public static DataTable GenerateStaticTestData()
        {
            DataTable result = new DataTable();

            DataColumn dc1 = new DataColumn();
            dc1.ColumnName = "ID";
            DataColumn dc2 = new DataColumn();
            dc2.ColumnName = "CompanyID";
            DataColumn dc3 = new DataColumn();
            dc3.ColumnName = "UserName";
            DataColumn dc4 = new DataColumn();
            dc4.ColumnName = "CompanyName";

            result.Columns.Add(dc1);
            result.Columns.Add(dc2);
            result.Columns.Add(dc3);
            result.Columns.Add(dc4);


            DataRow dr1 = result.NewRow();
            dr1["ID"] = 1;
            dr1["CompanyID"] = 1;
            dr1["UserName"] = "user1 Live";
            dr1["CompanyName"] = "Company1 Live";

            DataRow dr2 = result.NewRow();
            dr2["ID"] = 2;
            dr2["CompanyID"] = 1;
            dr2["UserName"] = "user2 Live";
            dr2["CompanyName"] = "Company1 Old";

            DataRow dr3 = result.NewRow();
            dr3["ID"] = 3;
            dr3["CompanyID"] = 2;
            dr3["UserName"] = "user3 Old";
            dr3["CompanyName"] = "Company2 Live";

            DataRow dr4 = result.NewRow();
            dr4["ID"] = 4;
            dr4["CompanyID"] = 2;
            dr4["UserName"] = "user4 Old";
            dr4["CompanyName"] = "Company2 Old";

            DataRow dr5 = result.NewRow();
            dr5["ID"] = 5;
            dr5["CompanyID"] = 3;
            dr5["UserName"] = "SingleUser";
            dr5["CompanyName"] = "Single Company";

            result.Rows.Add(dr1);
            result.Rows.Add(dr2);
            result.Rows.Add(dr3);
            result.Rows.Add(dr4);
            result.Rows.Add(dr5);

            return result;
        }

        //Create a test data table defined by the field list supplied to the method
        public static DataTable GenerateDynamicTestData(FieldCollection requiredFields, int NumberOfRows)
        {
            //Build columns
            DataTable dataSet = new DataTable();
            foreach (FieldItem fi in requiredFields.fieldList)
            {
                //Prevent Error from creating duplicate column names
                if (!dataSet.Columns.Contains(fi.FieldName))
                {
                    dataSet.Columns.Add(fi.FieldName);
                }
            }

            //Populate Data
            for (int i = 0; i < NumberOfRows; i++)
            {
                DataRow dr = dataSet.NewRow();
                for (int j = 0; j < dataSet.Columns.Count; j++)
                {
                    DataColumn dc = dataSet.Columns[j];
                    dr.SetField(dc, dc.ColumnName);
                }

                dataSet.Rows.Add(dr);
            }

            return dataSet;
        }

        public static void Log(String data, String fileNameAndPath)
        {
            using (StreamWriter sw = File.AppendText(fileNameAndPath))
            {
                sw.WriteLine(data);
            }
        }
        
        //This method designed to be called in a loop
        public static DataBlock getReportDataDeDup(DataTable AllRecordsTable, String FieldName, String FieldValue, SpreadSheet.FieldCollection fc, params String[] FieldsForUnique)
        {
            DataTable uniqueTable = new DataTable();

            //Get unique records from original bulk datatable
            uniqueTable = getUniqueRecords(AllRecordsTable, FieldsForUnique);

            //Apply Filter to get subset of results for each worksheet
            uniqueTable = getFilteredRecords(uniqueTable, FieldName, FieldValue);

            //return an object that can be passed straight to the report
            DataBlock result = DataBlock.createEnbuDataObject(uniqueTable, fc);
            return result;

        }

        //This method designed to be called in a loop
        public static DataBlock getReportData(DataTable AllRecordsTable, String FieldName, String FieldValue, SpreadSheet.FieldCollection fc)
        {
            DataTable filteredTable = new DataTable();

            //Apply Filter to get subset of results for each worksheet
            filteredTable = getFilteredRecords(AllRecordsTable, FieldName, FieldValue);

            //return an object that can be passed straight to the report
            DataBlock result = DataBlock.createEnbuDataObject(filteredTable, fc);
            return result;

        }

        public static DataTable getUniqueRecords(DataTable results, params String[] Field)
        {
            DataView view = new DataView(results);
            DataTable distinctValues = view.ToTable(true, Field);
            return distinctValues;
        }

        public static DataTable getFilteredRecords(DataTable result, String FieldName, String FieldValue)
        {
            //Used to get a subset of a datatable into a new DataTable

            string expression;
            expression = FieldName + " = '" + FieldValue + "'";

            DataRow[] foundRows;
            // Use the Select method to find all rows matching the filter.
            foundRows = result.Select(expression);

            DataTable filteredResults = new DataTable();
            filteredResults = result.Clone();
                       
            for (int i = 0; i < foundRows.Length; i++)
            {
                filteredResults.ImportRow(foundRows[i]);
            }

            return filteredResults;
        }

        public static void addReportData(SpreadSheet.EnbuReport eR, DataBlock edo, String sWorkSheetName)
        {
            if (edo.dataTable.Rows.Count > 0)
            {
                //Add DataTable to WorkSheet
                eR.AddDataObjectToWorkSheet(edo, sWorkSheetName);
            }
        }

        public static void createGroupsBy(SpreadSheet.EnbuReport eR, DataBlock edo, String WorkSheetName, String GroupByField, Boolean showColumnsHeadings = true)
        {
            //Get list of Unique Comm Types in the dataTable
            //Get groups of Data to work with
            var groupedData = from b in edo.dataTable.AsEnumerable()
                              group b by b.Field<string>(GroupByField) into g
                              select new
                              {
                                  FieldValue = g.Key,
                              };

            if (groupedData.Count() > 0)
            {
                //Go through each group and bring relevant rows into a new dataset
                foreach (var group in groupedData.AsEnumerable())
                {
                    DataTable subSet = new DataTable();
                    //Select The subrecords for one Master record and process them before adding them to the results Datatable
                    subSet = edo.dataTable.AsEnumerable()
                                    .Where(r => r.Field<string>(GroupByField) == group.FieldValue)
                                    .CopyToDataTable();

                    //Build report Object with header for each group
                    DataBlock edo_Sub = DataBlock.createEnbuDataObject(subSet, edo.fieldCollection);
                    edo_Sub.titleString = group.FieldValue;
                    edo.showColumnHeaders = showColumnsHeadings;


                    //Add Data Object
                    eR.AddDataObjectToWorkSheet(edo_Sub, WorkSheetName);

                }
            }



        }

        //Compares columns in a Datatable (typically from a SQL query) and compares them to a list of required fields. Drops and sorts them as required to remove system fields and display user friendly column names
        public static void FormatColumns(DataBlock edo, Boolean includePreviousFields = false)
        {
            //Remove duplicate Fields
            edo.fieldCollection.fieldList = edo.fieldCollection.fieldList.GroupBy(i => i.FieldName).Select(g => g.First()).ToList();

            //Compare required DataTable Column list with required field list - remove invalid required fields
            edo.fieldCollection = checkRequiredFields(edo);

            DataTable results = new DataTable();

            //Build a new blank DataTable from the fieldlist - create 'previous' fields where appropriate
            foreach (FieldItem fi in edo.fieldCollection.fieldList)
            {
                //If the field is a previous value only add if required
                if (fi.FieldName.GetLast(5) == "_Prev")
                {
                    if (includePreviousFields)
                    {
                        //This only executes if this current field is a Previous field and previous fields are required
                        addDataColumn(results, fi.FieldName);
                    }
                }
                else
                {
                    addDataColumn(results, fi.FieldName);
                }
            }

            //Merge the Data from DT to the new table
            foreach (DataRow dr in edo.dataTable.Rows)
            {
                //Create New row in results table
                DataRow drNew = results.NewRow();

                //For ech Column in the field list populate the new row
                foreach (FieldItem fi in edo.fieldCollection.fieldList)
                {
                    if (edo.dataTable.Columns.Contains(fi.FieldName))
                    {
                        if (fi.FieldName.GetLast(5) == "_Prev") // If previous field value check if user has specified to include these
                        {
                            if (includePreviousFields)
                            {
                                DataColumn dc = edo.dataTable.Columns[fi.FieldName];
                                drNew[fi.FieldName] = dr[dc.ColumnName];
                            }
                        }
                        else  // Otherwise its a regular field so copy the field value
                        {
                            DataColumn dc = edo.dataTable.Columns[fi.FieldName];
                            drNew[fi.FieldName] = dr[dc.ColumnName];
                        }

                    }

                }

                results.Rows.Add(drNew);

            }

            edo.dataTable = results;

        }







        //Private Methods==================================================================================================================================================================

        //Creates a DataTable (typically one column, with 1 or two rows) into some HTML to display at the top of a rendered datatable
        private static String GetHTMLHeader(DataTable dt, String BackgroundColour = "#DEDEE7")
        {
            string html = "<table width='100%' cellpadding='4' style='border-collapse: collapse' id='results'>";

            foreach (DataRow dr in dt.Rows)
            {
                html += createHTMLrow(true, 0);
                html += createHTMLCell(true, dr[0].ToString(), true);
                html += "</tr>";
            }

            return html;
        }

        private static FieldCollection removeHiddenFields(FieldCollection fc)
        {
            FieldCollection newFc = new FieldCollection();
            foreach (FieldItem fi in fc.fieldList)
            {
                if (fi.Visible == true)
                {
                    newFc.fieldList.Add(fi);
                }
            }

            return newFc;
        }

        //Function to build a EnbuDataObejct (Datatable and FieldList) to contain the header
        private static DataBlock CreateHeaderPanelForWorkSheet(String Heading, FieldCollection fc, ArrayList AdditionalText = null)
        {
            //Create Table for results
            DataTable headerTable = new DataTable();

            //Create the Columns
            foreach (FieldItem fi in fc.fieldList)
            {
                if (fi.Visible)
                {
                    addDataColumn(headerTable, fi.FieldName);
                }
            }

            //Add First Row with main header
            DataRow dr = headerTable.NewRow();
            dr[0] = Heading;
            headerTable.Rows.Add(dr);

            if (AdditionalText != null)
            {
                //Loop through the rows and add data (Note: Plus ONE because there is a main heading as well as the sub headings)
                for (int y = 0; y < AdditionalText.Count; y++)
                {
                    DataRow dr1 = headerTable.NewRow();
                    dr1[0] = AdditionalText[y];
                    headerTable.Rows.Add(dr1);
                }
            }


            DataBlock edo = new DataBlock();
            edo.dataTable = headerTable;
            edo.fieldCollection = fc;
            edo.showPrevious = false;
            edo.showColumnHeaders = false;


            return edo;

        }
           
        private static void addDataColumn(DataTable dt, String ColumnName)
        {

            DataColumn dc = new DataColumn();
            dc.ColumnName = ColumnName;
            if (!dt.Columns.Contains(ColumnName))
            {
                dt.Columns.Add(dc);
            }
            

            ////Cound Columns with this Name to see if Duplicates are being created
            //int duplicateColumnCount = 0;
            
            //foreach (DataColumn dc in dt.Columns)
            //{
            //    if (dc.ColumnName == ColumnName)
            //    {
            //        duplicateColumnCount = duplicateColumnCount + 1;
            //    }

            //}
            
            //DataColumn dc1 = new DataColumn();
            //if (duplicateColumnCount > 0)
            //{
            //    dc1.ColumnName = ColumnName + (duplicateColumnCount + 1).ToString();
            //}
            //else
            //{
            //    dc1.ColumnName = ColumnName;                
            //}

            //dt.Columns.Add(dc1);

        }

        private static void CreateHeaderPanelExcel(ExcelWorksheet ews, DataBlock edo, String Heading, String StartingCell = "A1", ArrayList AdditionalText = null)
        {
            addWorksheetData(ews, edo, StartingCell, false);
        }

        private static void addWorksheetData(ExcelWorksheet ews, DataBlock edo, String StartingCell, Boolean FormatOutput = true, String columnHeaderColour = "#B3B3B3", String rowColour1 = "#DEDEE7", String rowColour2 = "#FFFFFF", String titleColour = "#366092")
        {
            #region Data Column Validation (remove duplicates and hidden fields)
            //format the data to remove unwanted fields
            SpreadSheet.FormatColumns(edo, edo.showPrevious);
                       

            //Rename Datatable Columns to user the user friendly names instead of the field names
            foreach (DataColumn dc in edo.dataTable.Columns)
            {
                String updatedColumnHeading = getColumnHeader(dc.ColumnName, edo.fieldCollection.fieldList);
                if(edo.dataTable.Columns.Contains(updatedColumnHeading))
                {
                    //If column already Exists, get the amount of those columns and create a new one with cound at the end of the name
                    int colCount = 0;
                    foreach(DataColumn dctemp in edo.dataTable.Columns)
                    {
                        if(dctemp.ColumnName==updatedColumnHeading) colCount = colCount + 1;
                    }
                    //Update the column name
                    dc.ColumnName = updatedColumnHeading + "_" + colCount.ToString();
                }
                else
                {
                    //Update the column name
                     dc.ColumnName = updatedColumnHeading;
                }               
            }

            //We need a field List without hidden fields for the column width lookup later
            FieldCollection fc_Temp = new FieldCollection();
            //remove Fields marked as hidden from the DataTable
            foreach (FieldItem fI in edo.fieldCollection.fieldList)
            {
                if (fI.Visible == false)
                {
                    try
                    {
                        edo.dataTable.Columns.Remove(fI.ColumnHeader);
                    }
                    catch
                    {
                    }
                }
                else
                {
                    //Add this non
                    fc_Temp.fieldList.Add(fI);
                }
            }

            #endregion

            #region Create Title Rows (edo.dataTable.rows contains the data rows and additional rows for the heading info)

            ArrayList titleRows = new ArrayList();     //For Formatting Reasons, track the row number for the headings
            int ColumnHeadingRow = 1;                       //For Formatting Reasons, track the column header Row

            DataTable finalTable = new DataTable(); //Need new dataTable to import title and column heading rows
            finalTable = edo.dataTable.Clone();

            //check for Title and add DataRows As Appropriate
            if (edo.titleString != "")
            {
                //Create new DataTable and start populating it with title rows first then append the data to it
                

                DataColumn dc = finalTable.Columns[0];

                //Create Main Heading               
                DataRow dr = finalTable.NewRow();
                dr[dc] = edo.titleString;
                finalTable.Rows.Add(dr);
                titleRows.Add(0);                      //Add Row number to list of rows to be formatted as title

                //Create SubHeading Rows As Appropriate
                for(int i = 1; i <= edo.SubTitles.Count ; i++)
                {
                    titleRows.Add(i);                  //Track title rows
                    DataRow drsub = finalTable.NewRow();
                    drsub[dc] = edo.SubTitles[i-1].ToString();
                    finalTable.Rows.Add(drsub);
                }
            }

            //Create Column Header Row if Applicable
            if (edo.showColumnHeaders)
            {
                if (edo.titleString != "")
                {
                    ColumnHeadingRow = edo.SubTitles.Count + 1; // This Row is to be formatted as a Column Header
                }
                else
                {
                    ColumnHeadingRow = 0;
                }
                
                DataRow drHeadings = finalTable.NewRow();
                foreach (DataColumn dc_heading in finalTable.Columns)
                {
                    drHeadings[dc_heading] = dc_heading.ColumnName;
                }

                finalTable.Rows.Add(drHeadings);
            }

            //Merge in the Data rows
            foreach (DataRow d in edo.dataTable.Rows)
            {
                finalTable.ImportRow(d);
            }

            edo.dataTable = finalTable;

            #endregion

            //Set the rowCount variable LESS the row where EPPlus would normally insert the column headers
            int rowCount = edo.dataTable.Rows.Count;
            
            //Get First Letter from Starting Cell property
            String sStartingColumn = Convert.ToString(StartingCell[0]);
            int iRowOffset = Convert.ToInt16(StartingCell.Substring(1)); //get the number part of the starting cell as an int
            int iColumnOffset = ((int)StartingCell[0] % 32); //convert the letter to a column number

            //Setup Parameters for output
            int rows = rowCount;
            int columns = edo.dataTable.Columns.Count;
            int headerFontSize = 11;
            int dataFontSize = 10;

            //Create colours for the hex values from SAGE
            ColorConverter cc = new ColorConverter();
            Color headerColor = (Color)cc.ConvertFromString(columnHeaderColour);
            Color data1Color = (Color)cc.ConvertFromString(rowColour1);
            Color data2Color = (Color)cc.ConvertFromString(rowColour2);
            Color titleColor = (Color)cc.ConvertFromString(titleColour);

            ews.Cells.Style.Indent = 1;


            //Load Data Table to Spreadsheet - NEVER create the field headings, we manually do that
            ews.Cells[StartingCell].LoadFromDataTable(edo.dataTable, false);
            
            //Format output if required
            if (FormatOutput)
            {
                if (rowCount > 0)
                {
                    //Format all Rows in the Range                    
                    for (int currentRow = 0; currentRow <= rows - 1; currentRow++)
                    {
                        //get Starting Cell and Calculate ranage based on the number of columns
                        String sRowStartCell = sStartingColumn + (currentRow + iRowOffset).ToString();
                        String sRange = calculateRange(sRowStartCell, 0, edo.dataTable.Columns.Count); //The zero here means the range is only on a single row

                        if (titleRows.Contains(currentRow))
                        {
                            //Format this row as a Title or sub-title
                            if (currentRow == 0)
                            {
                                formatDataCells(ews, sRange, titleColor, titleColor, headerFontSize, currentRow);
                                ews.Cells[sRange].Merge = true;
                            }
                            else
                            {
                                formatDataCells(ews, sRange, titleColor, titleColor, dataFontSize, currentRow);
                                ews.Cells[sRange].Merge = true;
                            }
                            ews.Cells[sRange].Style.Border.BorderAround(ExcelBorderStyle.None);
                        }
                        else if (ColumnHeadingRow == currentRow)
                        {
                            //Format this row as Column Heading Row
                            formatDataCells(ews, sRange, headerColor, headerColor, dataFontSize, currentRow);
                        }
                        else
                        {
                            //Format a normal Row
                            formatDataCells(ews, sRange, data2Color, data1Color, dataFontSize, currentRow);
                        }

                    }

                    //Starting point for the overall formatting
                    String CurrentColumnStart = StartingCell;

                    if (titleRows.Count > 0)
                    {
                        //Get the title Range
                        String titleRange = calculateRange(CurrentColumnStart, titleRows.Count - 1, edo.dataTable.Columns.Count);
                        //Format the titles rows
                        using (var dRange = ews.Cells[titleRange])
                        {
                            dRange.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            dRange.Style.Font.Color.SetColor(Color.Black);
                            dRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            dRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            dRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            dRange.Style.Fill.BackgroundColor.SetColor(titleColor);
                            dRange.Style.WrapText = true;
                        }
                        
                        //Get new starting point
                        CurrentColumnStart = calculateNewStartingPosition(CurrentColumnStart, titleRows.Count, 0, 0);
                    }
                    

                    //Draw Borders around Columns for Data and Column Header Rows (adjust depending on number of tiles and column header visibility
                    for (int i = 0; i < edo.dataTable.Columns.Count; i++)
                    {
                        String ColumnRange = calculateRange(CurrentColumnStart, (edo.dataTable.Rows.Count - titleRows.Count - 1), 1);
                        using (var dRange = ews.Cells[ColumnRange])
                        {
                            dRange.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        }

                        CurrentColumnStart = calculateNewStartingPosition(CurrentColumnStart, 0, 1, 0);
                    }
                }

            }

            #region Resize Columns
            //Remove Hidden Fields to get the correct index of the column
            FieldCollection fcTemp = edo.fieldCollection;
            //Resize Columns
            for (int k = 0; k < columns; k++)
            {
                //fcTemp is a filedlist without hidden fields (defined above)
                FieldItem fI = fcTemp.fieldList[k];
                ews.Column(k + iColumnOffset).Width = fI.columnWidth;
            }
            #endregion


        }
        
        private static String calculateRange(String StartingCell, int rows, int columns)
        {
            int iColumnOffset = ((int)StartingCell[0] % 32) + columns - 1;       //Convert column to an int and add the number of columns to find the end range
            int iRowOffset = Convert.ToInt16(StartingCell.Substring(1));        //Convert row value to an int

            String EndCell = getColumnLetterFromIndex(iColumnOffset) + (iRowOffset + rows).ToString();

            return StartingCell + ":" + EndCell;
        }

        private static String calculateNewStartingPosition(String OldStartingCell, int rowOffset, int columnOffset, int spacing = 1)
        {
            String sStartingColumn = Convert.ToString(OldStartingCell[0]); // get the first letter on its own

            int iRowOffset = Convert.ToInt16(OldStartingCell.Substring(1)); //get the number part of the starting cell as an int
            int iColumnOffset = ((int)OldStartingCell[0] % 32) + columnOffset; //convert the letter to a column number

            String newColumnStart = getColumnLetterFromIndex(iColumnOffset);
            String newStartingCell = newColumnStart + (iRowOffset + rowOffset + spacing).ToString();

            return newStartingCell;
        }

        private static void formatHeaderCell(ExcelWorksheet ws, String SelectedCell, Color HeaderColour, int fontSize)
        {
            using (var dRange = ws.Cells[SelectedCell])
            {

                dRange.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                dRange.Style.Font.Color.SetColor(Color.White);
                dRange.Style.Font.Size = fontSize;
                dRange.Style.Font.Name = "sans-serif";
                dRange.Style.Font.Bold = false;
                dRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                dRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                dRange.Style.Font.UnderLine = false;
                dRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                dRange.Style.Fill.BackgroundColor.SetColor(HeaderColour);
                dRange.Style.WrapText = true;

            }
        }
        
        private static void formatDataCells(ExcelWorksheet ws, String SelectedCells, Color data1Colour, Color data2Colour, int fontSize, int rowIndex)
        {

            using (var dRange = ws.Cells[SelectedCells])
            {
                dRange.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                dRange.Style.Font.Color.SetColor(Color.Black);
                dRange.Style.Font.Size = fontSize;
                dRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                dRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                dRange.Style.Font.UnderLine = false;
                dRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                if (IsOdd(rowIndex))
                {
                    dRange.Style.Fill.BackgroundColor.SetColor(data1Colour);
                }
                else
                {
                    dRange.Style.Fill.BackgroundColor.SetColor(data2Colour);
                }

                dRange.Style.WrapText = true;
            }
        }

        static string getColumnLetterFromIndex(int index)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var value = "";

            if (index >= letters.Length)
                value += letters[index / letters.Length - 1];

            value += letters[(index % letters.Length) - 1];

            return value;
        }

        private static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }

        private static Boolean isColumnVisible(String fieldName, List<FieldItem> outputColumnList)
        {
            FieldItem result = outputColumnList.FirstOrDefault(s => s.FieldName == fieldName);
            if (result != null)
            {
                return result.Visible;
            }
            else
            {
                return false;
            }

        }

        private static String getColumnHeader(String fieldName, List<FieldItem> outputColumnList)
        {
            FieldItem result = outputColumnList.FirstOrDefault(s => s.FieldName == fieldName);
            if (result != null)
            {
                return result.ColumnHeader;
            }
            else
            {
                return fieldName;
            }
        }

        private static String createHTMLrow(Boolean isHeader, int rowIndex, String rowColour1 = "#DEDEE7", String rowColour2 = "#F7F7F7")
        {
            //Determine row colour
            String selectedColour = rowColour1;
            if (IsOdd(rowIndex))
            {
                selectedColour = rowColour2;
            }

            //Build row string
            String result = "";
            if (isHeader)
            {
                result = "<tr style='background:" + selectedColour + "' >";
            }
            else
            {
                result = "<tr style='background:" + selectedColour + "'>";
            }

            return result;
        }

        private static String createHTMLCell(Boolean isHeader, String TextValue, Boolean visibleField, int ColumnSpan = 1)
        {
            String result = "";
            String className = "REPORTDETAIL";
            if (isHeader)
            {
                className = "REPORTCOLUMNHEADER";
            }


            if (visibleField)
            {
                result = "<td class='" + className + "' colspan='" + ColumnSpan + "'>" + TextValue + "</td>";
            }
            else
            {
                result = "<td class='" + className + "' style='display:none;' colspan='" + ColumnSpan + "'>" + TextValue + "</td>";
            }


            return result;

        }

        public static DataTable ReverseRowsInDataTable(DataTable inputTable)
        {
            DataTable outputTable = inputTable.Clone();
            for (int i = inputTable.Rows.Count - 1; i >= 0; i--)
            {
                outputTable.ImportRow(inputTable.Rows[i]);
            }
            return outputTable;
        }

        private static FieldCollection checkRequiredFields(DataBlock edo)
        {
            //Create field list excluding TYPOs and DUPLICATES
            FieldCollection newFieldsList = new FieldCollection();
            foreach (FieldItem fI in edo.fieldCollection.fieldList)
            {
                //Always include "_Prev" fields in case they are required
                if (fI.FieldName.GetLast(5) == "_Prev")
                {
                    newFieldsList.InsertField(fI.FieldName, fI.ColumnHeader, fI.Visible);
                }
                else
                {
                    //Check that the required field is in the datatable - if not then drop it from the list
                    if (edo.dataTable.Columns.Contains(fI.FieldName))
                    {
                        newFieldsList.InsertField(fI.FieldName, fI.ColumnHeader, fI.Visible, fI.PreviousValue, fI.columnWidth);
                    }
                }

            }

            return newFieldsList;
        }

        public class FieldCollection
        {
            public List<FieldItem> fieldList = new List<FieldItem>();
            public void InsertField(String _fieldName, String _columnHeader, Boolean _visible = true, Boolean _previousValue = false, int colWidth = 30, Boolean showColumnHeaders = true)
            {
                FieldItem fI = new FieldItem();
                fI.FieldName = _fieldName;
                fI.ColumnHeader = _columnHeader;
                fI.Visible = _visible;
                fI.PreviousValue = _previousValue;
                fI.columnWidth = colWidth;
                fieldList.Add(fI);

                if (_previousValue)
                {
                    FieldItem fI2 = new FieldItem();
                    fI2.FieldName = _fieldName + "_Prev";
                    fI2.ColumnHeader = _columnHeader + " (Previous)";
                    fI2.Visible = _visible;
                    fI2.PreviousValue = false;
                    fI2.columnWidth = colWidth;
                    fieldList.Add(fI2);
                }

            }
            public void ClearFields()
            {
                fieldList.Clear();
            }

        }
        public class EnbuReport
        {
            private List<EnbuWorkSheet> _workSheets = new List<EnbuWorkSheet>();
            public List<EnbuWorkSheet> WorkSheets
            {
                get { return _workSheets; }
                set { _workSheets = value; }
            }

            public void CreateWorkSheet(String WorkSheetName, String StartingCell = "A1")
            {
                EnbuWorkSheet ews1 = new EnbuWorkSheet();
                ews1.DataObjects = new List<DataBlock>();
                ews1.workSheetname = WorkSheetName;
                ews1.startingCell = StartingCell;
                WorkSheets.Add(ews1);
            }

            public void AddDataObjectToWorkSheet(DataBlock edo, String worksheetName)
            {
                //Lookup the correct worksheet from the report worksheet collection
                EnbuWorkSheet ews = (from c in WorkSheets where c.workSheetname == worksheetName select c).FirstOrDefault();
                if (ews != null)
                {
                        ews.DataObjects.Add(edo);                                  
                }
            }

            public String ExportToHTML(String WorkSheetSeperator = "")
            {
                String result = SpreadSheet.ConvertReportToHTML(WorkSheets, WorkSheetSeperator);
                return result;
            }

            public byte[] ExportToExcel(String StartingCell, Boolean formatOutput, int Spacing = 1)
            {
                byte[] results = SpreadSheet.ConvertReportToExcel(WorkSheets, formatOutput, StartingCell, Spacing);
                return results;
            }
                        

        }


    }


    public class EnbuWorkSheet //Represents a collection of datatables/fields to be send to an excel worksheet
    {
        private List<DataBlock> _dataObjects = new List<DataBlock>();
        public List<DataBlock> DataObjects
        {
            get { return _dataObjects; }
            set { _dataObjects = value; }
        }
        public String workSheetname { get; set; }
        public String startingCell { get; set; }    //global Offset

        public void AddDataObject(DataTable dt, SpreadSheet.FieldCollection fc, Boolean isTitle = false, Boolean includePrevious = false)
        {
            DataBlock edo = new DataBlock();
            edo.dataTable = dt;
            edo.fieldCollection = fc;
            edo.showPrevious = includePrevious;
            DataObjects.Add(edo);
        }
    }

    public class DataBlock // A datatable and associated field lists
    {

        public DataBlock()
        {
            //initialise and set defaults
            showPrevious = false;
            showColumnHeaders = true;
            dataTable = new DataTable();
            fieldCollection = new SpreadSheet.FieldCollection();
            titleString = "";
            SubTitles = new ArrayList();            
        }

        public DataTable dataTable { get; set; }
        public SpreadSheet.FieldCollection fieldCollection { get; set; }
        public Boolean showColumnHeaders { get; set; }
        public Boolean showPrevious { get; set; }
        public String titleString { get; set; }
        public ArrayList SubTitles { get; set; }
                    
        public void CalculatePreviousValues(String GroupByField, String OrderByField, Boolean sortAscending = true, Boolean combineRows = true)
        {
            DataTable dt_withPrevious = new DataTable();
            
            if (this.dataTable != null)
            {
                DataBlock edo_Temp = DataBlock.createEnbuDataObject(this.dataTable, this.fieldCollection);
                dt_withPrevious = GetPreviousValues(edo_Temp, GroupByField, OrderByField, sortAscending, combineRows);                
            }

            this.dataTable = dt_withPrevious;
        }

        public static DataBlock createEnbuDataObject(DataTable dt, SpreadSheet.FieldCollection fc)
        {            
            DataBlock edo = new DataBlock();
            edo.dataTable = dt;
            edo.fieldCollection = fc;
            return edo;
        }

        
        //Process a Datatable - grouping by a field and sorting to get the top 2 values in a group - a new datatable is created combining the top two values into a single row
        //based on whether or not the fields should display a previous value
        private static DataTable GetPreviousValues(DataBlock edo, String GroupByField, String OrderByField, Boolean sortAscending = true, Boolean combineRows = true)
        {
            
            //Remove unneeded columns
            SpreadSheet.FormatColumns(edo, true);
            DataTable filteredResults = new DataTable();
            filteredResults = edo.dataTable.Clone();

            //Check GroupBy and OrderBy fields to make sure that those Columns exist
            if (edo.dataTable.Columns.Contains(GroupByField) && edo.dataTable.Columns.Contains(OrderByField))
            {

                //Get groups of Data to work with
                var groupedData = from b in edo.dataTable.AsEnumerable()
                                  group b by b.Field<string>(GroupByField) into g
                                  select new
                                  {
                                      FieldValue = g.Key,
                                      Count = g.Count()
                                  };

                if (groupedData.Count() > 0)
                {
                    //Go through each group and bring relevant rows into a new dataset
                    foreach (var group in groupedData.AsEnumerable())
                    {
                        int iterationCount = 2; //We are getting a previous value so take the top two results in a subSet of the data
                        DataTable subSet = new DataTable();

                        //Check orderBy for results (to determine which is the latest record
                        if (sortAscending)
                        {
                            //Select The subrecords for one Master record and process them before adding them to the results Datatable
                            subSet = edo.dataTable.AsEnumerable().OrderBy(n => n.Field<string>(OrderByField))
                                            .Where(r => r.Field<string>(GroupByField) == group.FieldValue)
                                            .CopyToDataTable();
                        }
                        else
                        {
                            //Select The subrecords for one Master record and process them before adding them to the results Datatable
                            subSet = edo.dataTable.AsEnumerable().OrderByDescending(n => n.Field<string>(OrderByField))
                                            .Where(r => r.Field<string>(GroupByField) == group.FieldValue)
                                            .CopyToDataTable();
                        }

                        //make sure you are not looping through more records then are available
                        if (iterationCount > subSet.Rows.Count) iterationCount = subSet.Rows.Count;

                        for (int i = 0; i < iterationCount; i++)
                        {
                            if (i == 0 && subSet.Rows.Count == 1)
                            {
                                DataRow singleRow = subSet.Rows[i];
                                //Check fields for ones that require previous values
                                foreach (DataColumn dc in subSet.Columns)
                                {
                                    if (dc.ColumnName.GetLast(5) == "_Prev")
                                    {
                                        singleRow[dc.ColumnName] = "No Previous Record";
                                    }
                                }
                                filteredResults.ImportRow(singleRow);
                            }
                            else if (i == 1)
                            {
                                //retrieve the older value and update the latest record before importing the latest record
                                DataRow latestRecord = subSet.Rows[i - 1];
                                DataRow historyRecord = subSet.Rows[i];

                                //Check fields for ones that require previous values
                                foreach (DataColumn dc in subSet.Columns)
                                {
                                    if (dc.ColumnName.GetLast(5) == "_Prev")
                                    {
                                        latestRecord[dc.ColumnName] = historyRecord[dc.ColumnName.Remove(dc.ColumnName.Length - 5)];
                                    }
                                }

                                //move the row into the results table
                                filteredResults.ImportRow(subSet.Rows[i - 1]);
                            }
                            else
                            {
                                //Do nothing with older records as we are only dealing with row[0] (latest) and row[1] (next latest)
                            }
                        }

                    }

                }

            }

            //Update dataTable
            return filteredResults;            
        }

    }

    public class FieldItem
    {
        public FieldItem()
        {
        }

        public String FieldName { get; set; }
        public String ColumnHeader { get; set; }
        public bool Visible { get; set; }
        private Boolean _previousValue = false;
        public Boolean PreviousValue
        {
            get { return _previousValue; }
            set { _previousValue = value; }
        }
        public int columnWidth { get; set; }


    }

    public static class StringExtension
    {
        public static string GetLast(this string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }
    }

}


