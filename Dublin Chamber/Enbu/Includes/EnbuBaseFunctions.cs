﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Win32;
using System.Text;
using Dublin_Chamber.eWare;

namespace Dublin_Chamber.Enbu.Includes
{
    public class EnbuBaseFunctions
    {
        /// <summary>
        /// Gets the CRM version from the registry (expects x64)
        /// </summary>
        /// <returns></returns>
        public string getCRMVersion()
        {
            eWareConnector eWareConn = new eWareConnector();
            string sInstallName = eWareConn.getInstallName(HttpContext.Current.Request.ServerVariables["URL"]);

            RegistryKey pRegKey = Registry.LocalMachine;

            pRegKey = pRegKey.OpenSubKey("SOFTWARE\\Wow6432Node\\eWare\\Config\\/" + sInstallName + "");
            ASCIIEncoding enc = new ASCIIEncoding();

            return pRegKey.GetValue("Version").ToString();
        }
    }
}