﻿using eWare;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dublin_Chamber.Enbu.Includes
{
    public class EnbuUIFunctions
    {
        eWareBase eWare;// = Dublin_Chamber.eWare.eWareConnector.Init();
        Enbu.Includes.EnbuBaseFunctions EnbuBase;
        Dublin_Chamber.eWare.eWareConnector eWareConn;
        /// <summary>
        /// Requires the eWareBase object in constructor
        /// </summary>
        /// <param name="eWareObject">eWareBase object.</param>
        public EnbuUIFunctions(eWareBase eWareObject, Enbu.Includes.EnbuBaseFunctions EnbuBaseIn, Dublin_Chamber.eWare.eWareConnector eWareConnIn)
        {
            eWare = eWareObject;
            EnbuBase = EnbuBaseIn;
            eWareConn = eWareConnIn;
        }

        /// <summary>
        /// Enbu function to create a CRM panel
        /// </summary>
        /// <param name="sTitle">Title of Panel</param>
        /// <param name="contentArr">ArrayList of content.</param>
        /// <param name="numCols">Number of columns to be created.</param>
        /// <param name="sNote">Any notes required.</param>
        /// <param name="sTableAtt">Any additional table attributes or styles.</param>
        /// <returns></returns>
        public String enbu_UI_draw_panel(String sTitle, ArrayList contentArr, int numCols, String sNote, String sTableAtt)
        {
            String CRMversion = EnbuBase.getCRMVersion();
            Dublin_Chamber.eWare.eWareConnector eWareConn = new Dublin_Chamber.eWare.eWareConnector();

            String sInstallName = eWareConn.getInstallName(HttpContext.Current.Request.ServerVariables["URL"]);

            if (sTableAtt != null) sTableAtt = "";


            String imgPath = enbu_UI_get_Theme_Path();

            String sTable = "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 " + sTableAtt + " >";

            if (CRMversion != "7.3")
                sTable +=
                    "<TR><TD VALIGN=BOTTOM>" +
                    "<IMG SRC='/'" + sInstallName + imgPath + "/backgrounds/paneleftcorner.jpg' HSPACE=0 BORDER=0 ALIGN=TOP></TD>" +
                    "<TD NOWRAP=true WIDTH=10% CLASS=PANEREPEAT>" + sTitle + "&nbsp;&nbsp;" +
                    "<A HREF=\"javascript:toggle(\'" + sTitle + "\')\">" +
                    "<IMG WIDTH=20 HEIGHT=20 SRC='/" + sInstallName + imgPath + "/buttons/componentstart.gif' BORDER=0 >" +
                    "</A>" +
                    "</TD>" +
                    "<TD VALIGN=BOTTOM CLASS=TABLETOPBORDER><IMG SRC='/" + sInstallName + imgPath + "/backgrounds/panerightcorner.gif' HSPACE=0 BORDER=0 ALIGN=TOP></TD>" +
                    "<TD CLASS=TABLETOPBORDER WIDTH=90% ALIGN=BOTTOM>&nbsp;</TD></TR>" +
                    "<TR CLASS=CONTENT><TD CLASS=TABLEBORDERLEFT COLSPAN=3>&nbsp;</TD><TD CLASS=TABLEBORDERRIGHT WIDTH=1>&nbsp;</TD></TR>";
            else
                sTable +=
                    "<TR><TD>&nbsp;</TD>" +
                    "<TD NOWRAP=true WIDTH=10% CLASS=PANEREPEAT>" + sTitle + "&nbsp;&nbsp;" +
                    "<A HREF=\"javascript:toggle(\'" + sTitle + "\')\">" +
                    "<IMG WIDTH=20 HEIGHT=20 SRC='/" + sInstallName + imgPath + "/buttons/componentstart.gif' BORDER=0 >" +
                    "</A>" +
                    "</TD>" +
                    "<TD CLASS=TABLETOPBORDER>&nbsp;</TD>" +
                    "<TD CLASS=TABLETOPBORDER WIDTH=90% ALIGN=BOTTOM>&nbsp;</TD></TR>" +
                    "<TR CLASS=CONTENT><TD CLASS=TABLEBORDERLEFT COLSPAN=3>&nbsp;</TD><TD CLASS=TABLEBORDERRIGHT WIDTH=1>&nbsp;</TD></TR>";

            if (sNote != null && sNote != "")
                sTable += "<TR CLASS=CONTENT><TD CLASS=TABLEBORDERLEFT>&nbsp;</TD>" +
                            "<TD CLASS=VIEWBOX COLSPAN=2><b>" + sNote + "</TD>" +
                            "<TD CLASS=TABLEBORDERRIGHT WIDTH=1>&nbsp;</TD></TR>";

            // content
            sTable += "<TR CLASS=CONTENT><TD CLASS=TABLEBORDERLEFT>&nbsp;</TD><TD COLSPAN=2 CLASS=VIEWBOX WIDTH=98% >" +
                        "<DIV STYLE='display:inline' ID='" + sTitle + "'>" +
                        "<TABLE WIDTH=100% border=0>";

            for (int i = 0; i < contentArr.Count; i++)
            {
                sTable += "<TR CLASS=CONTENT>";

                for (int j = 1; j <= numCols; j++)
                {
                    if (i < contentArr.Count)
                        sTable += "<TD VALIGN=TOP CLASS=VIEWBOX>" + contentArr[i++] + "</TD>";
                    else
                        sTable += "<TD>&nbsp;</TD>";
                }

                i--;

                sTable += "</TR>";
            }



            sTable += "</TABLE>" +
                        "</DIV>" +
                        "</TD><TD CLASS=TABLEBORDERRIGHT WIDTH=1>&nbsp;</TD></TR>" +
                // footer
                        "<TR CLASS=CONTENT><TD CLASS=TABLEBORDERLEFT COLSPAN=3>&nbsp;</TD><TD CLASS=TABLEBORDERRIGHT WIDTH=1>&nbsp;</TD></TR>" +
                        "<TR HEIGHT=1><TD CLASS=TABLEBORDERBOTTOM COLSPAN=4></TD></TR>" +
                        "</TABLE>";


            sTable +=
                        "<SCRIPT>" +
                        "function toggle(divId) {" +
                        "    oDiv = document.getElementById(divId);" +
                        "    if (oDiv.style.display=='inline'){" +
                        "        oDiv.style.display='none';" +
                        "    } else {" +
                        "       oDiv.style.display='inline';" +
                        "    }" +
                        "}" +
                        "</SCRIPT>";

            return sTable;


        }

        /// <summary>
        /// Returns a read only cell to be used in a DrawPanel. Creates the full TD and SPAN
        /// </summary>
        /// <param name="Caption">Caption of the SPAN</param>
        /// <param name="FieldName">Field name to give the span an id</param>
        /// <param name="Value">Value of the cell</param>
        /// <returns></returns>
        public static string GetReadOnlyCell(string Caption, string FieldName, string Value)
        {
            return "<td valign=\"TOP\"><span id=\"_Capt" + FieldName + "\" class=\"VIEWBOXCAPTION\">" + Caption + ":</span><br><span id=\"_Data" + FieldName + "\" class=\"VIEWBOX\">" + Value + "</span></td>";
        }

        public static string GetHeader(string HeaderTitle)
        {
            return "<td colspan=\"4\" align=\"center\" valign=\"TOP\"><br><hr><span id=\"_Capt\" class=\"VIEWBOXCAPTION\">" + HeaderTitle + ":</span><br><hr></td>";
        }

        /// <summary>
        /// Get the path of the themes folder. 
        /// </summary>
        /// <returns></returns>
        string enbu_UI_get_Theme_Path()
        {
            string imgPath = "/img";

            String sInstallName = eWareConn.getInstallName(HttpContext.Current.Request.ServerVariables["URL"]);
            // FM 3/5/12 - check for SS mode and set appropriately
            if (sInstallName == "")
            {
                //imgPath = sPortalName + imgPath;
            }

            var currentCRMVersion = "7.3";

            // need to sort out panel images due to themes
            if (currentCRMVersion != null)
            {
                if ((Convert.ToDouble(currentCRMVersion) >= 7))
                {
                    // get the current users theme
                    string currentUserId;
                    currentUserId = eWare.GetContextInfo("User", "User_UserId");
                    // FM 29/1/13 - handle default scenario
                    if (currentUserId == "") currentUserId = "1";

                    eWareRecord rsUserSettings = eWare.FindRecord("UserSettings", "Uset_Key = 'PreferredCssTheme' AND USet_UserId = " + currentUserId);
                    if (rsUserSettings.eof)
                    {
                        return "/Themes/img/default";
                    }
                    else
                    {
                        imgPath = "/Themes/img/" + rsUserSettings["Uset_Value"];
                    }
                }
                else
                {
                    return imgPath;
                }

            }
            return imgPath;
        }
    }
}