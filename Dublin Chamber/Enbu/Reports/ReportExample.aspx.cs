﻿using eWare;
using Dublin_Chamber.eWare;
using Dublin_Chamber;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dublin_Chamber.Enbu;



namespace Dublin_Chamber.Enbu
{
    public partial class ReportExample : System.Web.UI.Page
    {
        public EnbuHelper Enbu = new EnbuHelper();

        String outputOption;
        String sMemStart;
        String sMemEnd;
        String sEvntStart;
        String sEvntEnd;
        String sCompanyIDs;
        String sFormatOption;
        String sSpacingAmount;
        String sGroupHeadings;

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private String getSQLString(String EntityName, String sMemStart, String sMemEnd, String CompanyIDs, String sEvtStart, String sEvtEnd)
        {
            String result = "";
            String whereStr = "";

            //If company ID's are selected then make them a criteria of the SQL String
            if (CompanyIDs == "")
            {
                whereStr = "WHERE Membership.memb_enddate Between '" + sMemStart + " 00:00:00' and '" + sMemEnd + " 23:59:59' AND Membership.memb_Deleted is null ";
            }
            else
            {
                whereStr = "WHERE Membership.memb_enddate Between '" + sMemStart + " 00:00:00' and '" + sMemEnd + " 23:59:59' AND Membership.memb_Deleted is null " +
                                "AND Comp_CompanyID in (" + CompanyIDs + ")";
            }


            String personSQL = "SELECT Company.Comp_CompanyId, Person.Pers_PersonId, Person.Pers_FirstName + ' ' + Person.Pers_LastName As Person, " +
                                "Person.Pers_Title " +
                                "FROM         Company INNER JOIN " +
                                "Membership ON Company.Comp_CompanyId = Membership.memb_companyid INNER JOIN " +
                                "Person ON Company.Comp_CompanyId = Person.Pers_CompanyId " + whereStr + " AND Company.Comp_Deleted is null";

            String eventSQL = "SELECT     Company.Comp_CompanyId, Company.Comp_Name, Company.Comp_Type, Company.Comp_Status, Membership.memb_startdate, Membership.memb_enddate, " +
                              "Person.Pers_FirstName + ' ' + Person.Pers_LastName AS Person, Person.Pers_Title, Person.Pers_PersonId, Attendance.attd_date, Attendance.attd_status, " +
                              "Event.evnt_Name, Event.evnt_date, Event.evnt_status " +
                              "FROM         Event INNER JOIN " +
                              "Attendance ON Event.evnt_EventID = Attendance.attd_eventid INNER JOIN " +
                              "Company INNER JOIN " +
                              "Membership ON Company.Comp_CompanyId = Membership.memb_companyid INNER JOIN " +
                              "Person ON Company.Comp_CompanyId = Person.Pers_CompanyId ON Attendance.attd_personid = Person.Pers_PersonId " + whereStr +
                              " AND Attendance.attd_date BETWEEN '" + sEvtStart + " 00:00:00' and '" + sEvtEnd + " 23:59:59' AND attd_Deleted is null";

            String eventSQl2 = "SELECT even_Name, Comp_CompanyId, " +
                               "people = STUFF(( SELECT ', ' + Replace(Pers_FirstName,' ','') + ' ' + Replace(Pers_LastName,' ','') As Person FROM vEventAttendees " +
                               "WHERE even_Name = x.even_Name AND Comp_CompanyId = x.Comp_CompanyId " +
                               "FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') " +
                               "FROM vEventAttendeesList AS x " +
                               "WHERE Comp_CompanyId in (" + CompanyIDs + ") " +
                               "GROUP BY even_Name, comp_companyid " +
                               "order by Comp_CompanyId ";



            String companiesSQL = "SELECT Company.Comp_CompanyId, Company.Comp_Name, Company.Comp_Status, Membership.memb_membershipid, Membership.memb_startdate, Membership.memb_enddate " +
                                  "FROM Company INNER JOIN " +
                                  "Membership ON Company.Comp_CompanyId = Membership.memb_companyid " + whereStr + " AND Company.Comp_Deleted is null";


            String commsSQL =   "SELECT ,[Comm_Type],[Comm_Action],[Comm_Status],[Comm_Priority],[Comm_DateTime],[Comm_ToDateTime],[Comm_Note],[Comm_Deleted],[Comm_Description],[Comp_CompanyId], " +
                                "[Comp_Name] ,[Comp_Type] ,[Comp_Status] ,[comp_validto] FROM [CRM_Live].[dbo].[vCommCompMembership] " +
                                "WHERE comp_validto Between '" + sMemStart + " 00:00:00' and '" + sMemEnd + " 23:59:59' AND vCommunication.Comm_DateTime BETWEEN '" + sEvtStart + " 00:00:00' and '" + sEvtEnd + " 23:59:59' AND vCommunication.Comm_Deleted is null";




            if (EntityName == "Companies")
            {
                result = companiesSQL;
            }
            else if (EntityName == "Events")
            {
                result = eventSQl2;
            }
            else if (EntityName == "Persons")
            {
                result = personSQL;
            }
            else if (EntityName == "Communications")
            {
                result = commsSQL;
            }

            return result;
        }

        private ArrayList buildParameters()
        {


            String dateparams = "" +
                                "<table border=0>" +
                                "<tr><td></td><tdClass='VIEWBOXCAPTION'>Membership Date Range</td></tr>" +
                                "<tr><td>From</td><td><input type='Text' id='sStartDate_mem' name='_sStartDate_mem' class='datepicker' value='" + sMemStart + "'/></td></tr>" +
                                "<tr><td>To</td><td><input type='Text' id='sEndtDate_mem' name='_sEndDate_mem' class='datepicker' value='" + sMemEnd + "'/></td></tr>" +
                                "<tr><td><input type='hidden' id='selectedCompanies_names'/></td></tr>" +
                                "<tr><td><input type='hidden' id='selectedCompanies_ids' name='_selectedCompanies_ids'/></td></tr>" +
                                "<tr><td colspan=2><hr></td></tr>" +
                                "<tr><td colspan=2>Communication Date Range</td></tr>" +
                                "<tr><td>From</td><td><input type='Text' id='sStartDate_evt' name='_sStartDate_evt' class='datepicker' value='" + sEvntStart + "'/></td></tr>" +
                                "<tr><td>To</td><td><input type='Text' id='sEndtDate_evt' name='_sEndDate_evt' class='datepicker' value='" + sEvntEnd + "'/></td></tr>" +
                                "</table>";

            String compParams = "" +
                                "<table border=0>" +
                                "<tr><tdClass='VIEWBOXCAPTION'>Company Name <i>(Make Selection to Filter results)</i></td></tr>" +
                                "<tr><td><select multiple id='selectedCompanies' name='_selectedCompanies'  style='width: 250px' size=8></select></td></tr>" +
                                "<tr><td><input type='Text' id='sFilter' name='_sFilter' style='width: 250px'/><i>&nbspFilter</i></td></tr>" +
                                "</table>";

            String reportOptions = "" +
                                "<table border=0>" +
                                "<tr><td><br><b><i>Report Options</i></b></td><tr>" +
                                "<tr><td><select id='sOutput' name='_sOutput'  style='width: 300px'><option value='1'>Render to Screen</option><option value='2' selected>Export to Spreadsheet</option></select></td></tr>" +
                                "<tr><td><select id='sFormat' name='_sFormat'  style='width: 300px'><option selected value='1'>Formatted Output</option><option value='2'>No Formatting Applied</option></select></td></tr>" +
                                "<tr><td>Spacing Between Groups:</td></tr>" +
                                "<tr><td><select id='sSpacing' name='_sSpacing'  style='width: 50px'><option selected value='1'>1</option><option value='2'>2</option></option><option value='3'>3</option></select></td></tr>" +
                                "<tr><td>Headings on Communication Groups?</td></tr>" +
                                "<tr><td><select id='sgroupHeading' name='_sgroupHeading'  style='width: 50px'><option selected value='1'>Yes</option><option value='2'>No</option></select></td></tr>" +
                                "</table>";


            ArrayList contArry = new ArrayList();
            contArry.Add(compParams);
            contArry.Add(dateparams);
            contArry.Add(reportOptions);

            return contArry;
        }



    }
}